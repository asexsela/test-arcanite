import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Home from '../pages/main/Home.vue'
import Singin from '../pages/Singin.vue'
import Singup from '../pages/Singup.vue'
import NotFound from '../pages/errors/404.vue'

//main pages
import HomeMain from '../pages/main/HomeMain.vue'
import HomeProfile from '../pages/main/HomeProfile.vue'



// routes
const routes = [
    { path: '/', name: 'Singin', component: Singin, meta: {auth: false, redirect: '/'} },
    { path: '/register', name: 'Singup', component: Singup, meta: {auth: false, redirect: '/'} },

    { 
        path: '/main', name: 'Home', component: Home, meta: {auth: true},
        children: [
            {path: '/', name: 'HomeMain', component: HomeMain},
            {path: 'profile', name: 'HomeProfile', component: HomeProfile},
        ]
    },

    { path: '*', name: 'NotFound', component: NotFound,}
]

const router = new VueRouter({
    routes,
    hashbang: true,
    mode: 'history'
})


export default router