/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import 'vuetify/dist/vuetify.min.css'

import Vue from 'vue'
import axios from 'axios';
import VueAxios from 'vue-axios';
import router from './Router.js'
import AppHome from '../layouts/AppHome.vue'
import VueAuth from '@websanova/vue-auth'
import AuthBearer from '@websanova/vue-auth/drivers/auth/bearer.js'
import AuthHttp from '@websanova/vue-auth/drivers/http/axios.1.x.js'
import routerVueRouter from '@websanova/vue-auth/drivers/router/vue-router.2.x.js'
import Vuetify from '../plugins/vuetify'

Vue.use(VueAxios, axios);

axios.defaults.baseURL = 'http://test.loc/api';

window.Vue = require('vue');

Vue.router = router

Vue.use(VueAuth, {
    auth: AuthBearer,
    http: AuthHttp,
    router: routerVueRouter,
    authRedirect: {path: '/'},
    notFoundRedirect: {path: '/main'}
});

AppHome.router = Vue.router
AppHome.vuetify = Vuetify

new Vue(AppHome).$mount('#app');




