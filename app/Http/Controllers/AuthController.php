<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use JWTAuth;
use Auth;

class AuthController extends Controller
{

    public function register(RegisterRequest $request)
    {

        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = $request->password;
        $user->save();

        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {

            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Неверный email или пароль'
            ], 400);

        }

        return response([
            'status' => 'success'
        ])->header('Authorization', $token);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);

        return response([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function getUser()
    {
        return UserResource::collection(User::orderBy('created_at', 'desc')->get());
    }

    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }

    public function logout()
    {
        JWTAuth::invalidate();
        
        return response([
            'status' => 'success',
            'msg' => 'Вы успешно вышли'
        ], 200);
    }
}
